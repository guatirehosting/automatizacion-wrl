package web;

import com.everis.data.ExcelUtils;
import com.everis.global.Global;
import com.everis.po.LoginPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LoginTest extends Base{

    Global global = new Global(driver);

    @DataProvider
    public Object[][] LoginCases(ITestContext context) throws Exception{
        Object[][] testObjArray = ExcelUtils.getTableArray(super.getDataProviderDir()+context.getCurrentXmlTest().getParameter("dataProvider"),context.getCurrentXmlTest().getParameter("sheet"));
        return (testObjArray);
    }

    // LOGIN ADMINISTRADOR
    @Test(description="Caso de prueba login Admin", dataProvider="LoginCases")
    public void validarMensajeDeBienvenida(String username, String password, String msje) throws InterruptedException {

        LoginPage loginPage = new LoginPage(driver);

        //Act and Assert
        loginPage.executeLoginAdmin(username, password);
    }

    // LOGIN INGENIERO
    @Test(description="Caso de prueba login Ingeniero", dataProvider="LoginCases")
    public void validarMensajeDeBienvenidaIngeniero(String username, String password, String msje) throws InterruptedException {

        LoginPage loginPage = new LoginPage(driver);

        //Act and Assert
        loginPage.executeLoginIngeniero(username, password);
    }

    // LOGIN OPERADOR
    @Test(description="Caso de prueba login Operador", dataProvider="LoginCases")
    public void validarMensajeDeBienvenidaOperador(String username, String password, String msje) throws InterruptedException {

        LoginPage loginPage = new LoginPage(driver);

        //Act and Assert
        loginPage.executeLoginOperador(username, password);
    }

    // LOGIN FALLIDO
   @Test(description="Caso de prueba login fail", dataProvider="LoginCases")
    public void validarMensajeDeError(String username, String password) throws InterruptedException {

        LoginPage loginPage = new LoginPage(driver);

        loginPage.setUsername(username);
        loginPage.setPassword(password);
        loginPage.enviarLogin();

        String msjErrorLogin = loginPage.getmsjErrorLogin();
        Assert.assertEquals(msjErrorLogin, "Los datos de usuario / password ingresados son invalidos. Si esta seguro de haber ingresado bien los datos, es posible que este bloqueado el usuario.");

    }

}
