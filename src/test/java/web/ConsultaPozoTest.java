package web;

import com.everis.data.ExcelUtils;
import com.everis.global.Global;
import com.everis.po.BuscarIntervencionPage;
import com.everis.po.ConsultaPozoPage;
import com.everis.po.IndexPage;
import com.everis.po.LoginPage;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ConsultaPozoTest extends Base{

    Global global = new Global(driver);

    @DataProvider
    public Object[][] consultaPozoCases(ITestContext context) throws Exception{
        Object[][] testObjArray = ExcelUtils.getTableArray(super.getDataProviderDir()+context.getCurrentXmlTest().getParameter("dataProvider"),context.getCurrentXmlTest().getParameter("sheet"));
        return (testObjArray);
    }

    // BUSCAR HISTORIA DE POZO
    @Test(description="Consulta Historia de Pozo", dataProvider="consultaPozoCases")
    public void buscarHistoriaPozo(String username, String password, String distrito, String proyecto , String pozo) throws InterruptedException {

        //Arrange
        LoginPage loginPage = new LoginPage(driver);
        IndexPage indexpage = new IndexPage(driver);
        ConsultaPozoPage consultaPozoPage = new ConsultaPozoPage(driver);

        //Act
        loginPage.executeLoginIngeniero(username, password);
        indexpage.buscarConsultadePozo();
        consultaPozoPage.buscarHistoriaPozo(distrito, proyecto , pozo);


    }

}
