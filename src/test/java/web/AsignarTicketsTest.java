package web;

import com.everis.data.ExcelUtils;
import com.everis.global.Global;
import com.everis.po.IndexPage;
import com.everis.po.LoginPage;
import com.everis.po.TicketsPage;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class AsignarTicketsTest extends Base{

    Global global = new Global(driver);

    @DataProvider
    public Object[][] asignarTicketCases(ITestContext context) throws Exception{
        Object[][] testObjArray = ExcelUtils.getTableArray(super.getDataProviderDir()+context.getCurrentXmlTest().getParameter("dataProvider"),context.getCurrentXmlTest().getParameter("sheet"));
        return (testObjArray);
    }

    //ASIGNAR UNA ORDEN DE TRABAJO DE UN PROGRAMA UN EQUIPO PARA REALIZAR MANIOBRAS
    @Test(description="Asignar Equipo Offline", dataProvider="asignarTicketCases")

    public void asignarEquipoOffline(String username, String password, String funcionalidad , String intervencion, String equipo) throws InterruptedException {

        //Arrange
        LoginPage loginPage = new LoginPage(driver);
        IndexPage indexpage = new IndexPage(driver);
        TicketsPage ticketsPage = new TicketsPage(driver);

        //Act
        loginPage.executeLoginAdmin(username, password);
        indexpage.menuTickets();
        ticketsPage.asignarEquipoOffline(funcionalidad, intervencion, equipo);


    }
}
