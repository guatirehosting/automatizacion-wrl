package web;

import com.everis.data.ExcelUtils;
import com.everis.global.Global;
import com.everis.po.IndexPage;
import com.everis.po.IntervencionPage;
import com.everis.po.LoginPage;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class IntervencionTest extends Base{

    Global global = new Global(driver);

    @DataProvider
    public Object[][] intervencionCases(ITestContext context) throws Exception{
        Object[][] testObjArray = ExcelUtils.getTableArray(super.getDataProviderDir()+context.getCurrentXmlTest().getParameter("dataProvider"),context.getCurrentXmlTest().getParameter("sheet"));
        return (testObjArray);
    }

    // GENERAR INTERVENCIÓN
    @Test(description="Caso generar intervencion", dataProvider="intervencionCases")
    public void generarIntervencion(String username, String password, String opcionIntervencion, String opcionPozo, String tipoImp, String subTipo, String fecha, String comentarios, String TipoPrograma, String comentarioPrograma) throws InterruptedException {

        //Arrange
        LoginPage loginPage = new LoginPage(driver);
        IndexPage indexpage = new IndexPage(driver);
        IntervencionPage intervencionPage = new IntervencionPage(driver);

        //Actions
        loginPage.executeLoginIngeniero(username, password);
        indexpage.crearIntervencion();
        intervencionPage.generarIntervencion(opcionIntervencion, opcionPozo, tipoImp, subTipo, fecha, comentarios);

        //Assert Generar Intervencion
        String msjAlerta = intervencionPage.getMsjeAlerta();
        Assert.assertEquals(msjAlerta, "El registro ha sido guardado correctamente");
        intervencionPage.aceptarAlert();



    }


}
