package web;

import com.everis.data.ExcelUtils;
import com.everis.global.Global;
import com.everis.po.*;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class VerProgramaTest extends Base{

    Global global = new Global(driver);

    @DataProvider
    public Object[][] verProgramas(ITestContext context) throws Exception{
        Object[][] testObjArray = ExcelUtils.getTableArray(super.getDataProviderDir()+context.getCurrentXmlTest().getParameter("dataProvider"),context.getCurrentXmlTest().getParameter("sheet"));
        return (testObjArray);
    }

    // BUSCAR PROGRAMA
    @Test(description="Ver Programas", dataProvider="verProgramas")
    public void verProgramas(String username, String password, String estado, String opcionPozo, String tipoPrograma) throws InterruptedException {

        //Arrange
        LoginPage loginPage = new LoginPage(driver);
        IndexPage indexpage = new IndexPage(driver);
        VerProgramaPage verPrograma = new VerProgramaPage(driver);

        //Actions
        loginPage.executeLoginOperador(username, password);
        indexpage.verProgramas();
        verPrograma.buscarPrograma(estado, opcionPozo, tipoPrograma);
    }

    // ALTA PARTE DIARIO
    @Test(description="Alta Parte Diario", dataProvider="verProgramas")
    public void altaParteDiario(String username, String password, String estado, String tipoPrograma, String opcionPozo) throws InterruptedException {

        //Arrange
        LoginPage loginPage = new LoginPage(driver);
        IndexPage indexpage = new IndexPage(driver);
        VerProgramaPage verPrograma = new VerProgramaPage(driver);
        ProgramaPage programaPage = new ProgramaPage(driver);


        //Actions
        loginPage.executeLoginOperador(username, password);
        indexpage.verProgramas();
        verPrograma.buscarPrograma(estado, tipoPrograma, opcionPozo);
        programaPage.altaParteDiario();

        //Assert
        String msjAlertaParteDiario = programaPage.getMsjeAlerta();
        Assert.assertEquals(msjAlertaParteDiario, "El registro ha sido guardado correctamente");
        programaPage.aceptarAlert();

    }

    // FINALIZAR PROGRAMA
    @Test(description="Finalizar Programa", dataProvider="verProgramas")
    public void finalizarPrograma(String username, String password, String estado, String tipoPrograma, String opcionPozo, String estado2) throws InterruptedException {


        //Arrange
        LoginPage loginPage = new LoginPage(driver);
        IndexPage indexpage = new IndexPage(driver);
        VerProgramaPage verPrograma = new VerProgramaPage(driver);
        ProgramaPage programaPage = new ProgramaPage(driver);

        //Actions
        loginPage.executeLoginOperador(username, password);
        indexpage.verProgramas();
        verPrograma.buscarPrograma(estado, tipoPrograma, opcionPozo);
        programaPage.finalizarPrograma();
        programaPage.aceptarAlert();

        String msjAlertaGuardar = programaPage.getMsjeAlerta();
        Assert.assertEquals(msjAlertaGuardar, "El registro ha sido guardado correctamente");
        programaPage.aceptarAlert();

        indexpage.irABuscarIntervenciones();
        verPrograma.buscarProgramaFinalizado(estado2, tipoPrograma, opcionPozo);

    }

}