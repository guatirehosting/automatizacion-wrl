package web;

import com.everis.data.ExcelUtils;
import com.everis.global.Global;
import com.everis.po.*;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class AprobarIntervencionTest extends Base{

    Global global = new Global(driver);

    @DataProvider
    public Object[][] aprobarIntervencion(ITestContext context) throws Exception{
        Object[][] testObjArray = ExcelUtils.getTableArray(super.getDataProviderDir()+context.getCurrentXmlTest().getParameter("dataProvider"),context.getCurrentXmlTest().getParameter("sheet"));
        return (testObjArray);
    }

    // APROBAR INTERVENCIÓN CON USUARIO SUPERVISOR
    @Test(description="Aprobar Intervención", dataProvider="aprobarIntervencion")
    public void aprobarIntervencion(String username, String password, String opcionPozo, String estado, String estado2) throws InterruptedException {

        //Arrange
        LoginPage loginPage = new LoginPage(driver);
        IndexPage indexpage = new IndexPage(driver);
        BuscarIntervencionPage buscarIntervencionPage = new BuscarIntervencionPage(driver);
        IntervencionPage intervencionPage = new IntervencionPage(driver);
        ProgramaPage programaPage = new ProgramaPage(driver);

        //Actions
        loginPage.executeLoginSupervisor(username, password);
        indexpage.irABuscarIntervenciones();
        buscarIntervencionPage.buscarIntervencion(opcionPozo, estado);
        intervencionPage.editarProgramaAsociado();
        programaPage.aprobarIntervencion();
        programaPage.aceptarAlert();

        String msjAlertaGuardar = programaPage.getMsjeAlerta();
        Assert.assertEquals(msjAlertaGuardar, "El registro ha sido guardado correctamente");
        programaPage.aceptarAlert();

        indexpage.irABuscarIntervenciones();
        buscarIntervencionPage.buscarIntervencion(opcionPozo, estado2);

    }

    // CIERRE INTERVENCIÓN CON USUARIO INGENIERO
    @Test(description="Cierre Intervención", dataProvider="aprobarIntervencion")
    public void cierreIntervencion(String username, String password, String opcionPozo, String estado, String comentario, String estado2) throws InterruptedException {

        //Arrange
        LoginPage loginPage = new LoginPage(driver);
        IndexPage indexpage = new IndexPage(driver);
        BuscarIntervencionPage buscarIntervencionPage = new BuscarIntervencionPage(driver);
        IntervencionPage intervencionPage = new IntervencionPage(driver);


        //Actions
        loginPage.executeLoginIngeniero(username, password);
        indexpage.irABuscarIntervenciones();
        buscarIntervencionPage.buscarIntervencion(opcionPozo, estado);
        intervencionPage.finalizarIntervencion(comentario);

        //String msjAlertaGuardar = intervencionPage.getMsjeAlerta();
        //Assert.assertEquals(msjAlertaGuardar, "El registro ha sido guardado correctamente");
        //intervencionPage.aceptarAlert();

        /*indexpage.irABuscarIntervenciones();
        buscarIntervencionPage.buscarIntervencion(opcionPozo, estado2);*/

    }

    // FIN OPERATIVO DE UNA INTERVENCIÓN CON USUARIO SUPERVISOR
    @Test(description="Fin Operativo", dataProvider="aprobarIntervencion")
    public void finOperativo(String username, String password, String opcionPozo, String estado, String comentario) throws InterruptedException {

        //Arrange
        LoginPage loginPage = new LoginPage(driver);
        IndexPage indexpage = new IndexPage(driver);
        BuscarIntervencionPage buscarIntervencionPage = new BuscarIntervencionPage(driver);
        IntervencionPage intervencionPage = new IntervencionPage(driver);

        //Actions
        loginPage.executeLoginSupervisor(username, password);
        indexpage.irABuscarIntervenciones();
        buscarIntervencionPage.buscarIntervencion(opcionPozo, estado);
        intervencionPage.finOperativo(comentario);

        String msjAlertaGuardar = intervencionPage.getMsjeAlerta();
        Assert.assertEquals(msjAlertaGuardar, "El registro ha sido guardado correctamente");
        intervencionPage.aceptarAlert();

        /*indexpage.irABuscarIntervenciones();
        buscarIntervencionPage.buscarIntervencion(opcionPozo, estado2);*/

    }







}
