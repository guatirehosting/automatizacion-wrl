package web;

import com.everis.data.ExcelUtils;
import com.everis.global.Global;
import com.everis.po.*;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class AltaProgramaTest extends Base{

    Global global = new Global(driver);

    @DataProvider
    public Object[][] intervencionCases(ITestContext context) throws Exception{
        Object[][] testObjArray = ExcelUtils.getTableArray(super.getDataProviderDir()+context.getCurrentXmlTest().getParameter("dataProvider"),context.getCurrentXmlTest().getParameter("sheet"));
        return (testObjArray);
    }

    // ALTA DE PROGRAMA CON CREACION DE ORDEN DE TRABAJO Y APROBACION DEL PROGRAMA
    @Test(description="Caso Alta Programa", dataProvider="intervencionCases")
    public void altaPrograma(String username, String password, String opcionPozo, String estado, String TipoPrograma, String comentarioPrograma) throws InterruptedException {

        //Arrange
        LoginPage loginPage = new LoginPage(driver);
        IndexPage indexpage = new IndexPage(driver);
        IntervencionPage intervencionPage = new IntervencionPage(driver);
        BuscarIntervencionPage buscarIntervencionPage = new BuscarIntervencionPage(driver);
        ProgramaPage programaPage = new ProgramaPage(driver);

        //Actions: Dar de alta un Programa
        loginPage.executeLoginIngeniero(username, password);
        indexpage.irABuscarIntervenciones();
        buscarIntervencionPage.buscarIntervencion(opcionPozo, estado);
        intervencionPage.generarAltaPrograma();
        programaPage.altaPrograma(TipoPrograma, comentarioPrograma);

        //Assert
        String msjAlertaPrograma = programaPage.getMsjeAlerta();
        Assert.assertEquals(msjAlertaPrograma, "El registro ha sido guardado correctamente");
        programaPage.aceptarAlert();

        //Actions: Completar Ordenes de Trabajo
        programaPage.ordenesDeTrabajo();

        //Assert
        String msjAlertaPrograma2 = programaPage.getMsjeAlerta();
        Assert.assertEquals(msjAlertaPrograma2, "El registro ha sido guardado correctamente");
        programaPage.aceptarAlert();

        //Actions: Aprobar programa
        programaPage.aprobarPrograma();
        programaPage.aceptarAlert();

    }
}
