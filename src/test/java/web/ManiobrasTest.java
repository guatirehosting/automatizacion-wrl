package web;

import com.everis.data.ExcelUtils;
import com.everis.global.Global;
import com.everis.po.*;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ManiobrasTest extends Base{

    Global global = new Global(driver);

    @DataProvider
    public Object[][] verProgramas(ITestContext context) throws Exception{
        Object[][] testObjArray = ExcelUtils.getTableArray(super.getDataProviderDir()+context.getCurrentXmlTest().getParameter("dataProvider"),context.getCurrentXmlTest().getParameter("sheet"));
        return (testObjArray);
    }

    // CREAR MANIOBRA CHARLA SEGURIDAD
    @Test(description="Maniobra Charla Seguridad", dataProvider="verProgramas")
    public void crearCharlaSeguridad(String username, String password, String estado, String tipoPrograma, String opcionPozo, String opcionCharla, String horaInicio, String horaFin, String obserOperacion, String obserManiobra, String operador, String responsable, String ayudante1) throws InterruptedException {

        //Arrange
        LoginPage loginPage = new LoginPage(driver);
        IndexPage indexpage = new IndexPage(driver);
        VerProgramaPage verPrograma = new VerProgramaPage(driver);
        ProgramaPage programaPage = new ProgramaPage(driver);
        ManiobrasPage maniobrasPage = new ManiobrasPage(driver);


        //Actions
        loginPage.executeLoginOperador(username, password);
        indexpage.verProgramas();
        verPrograma.buscarPrograma(estado, tipoPrograma, opcionPozo);
        programaPage.altaParteDiario();

        //Assert Alerta Parte Diario
        String msjAlertaParteDiario = programaPage.getMsjeAlerta();
        Assert.assertEquals(msjAlertaParteDiario, "El registro ha sido guardado correctamente");
        programaPage.aceptarAlert();

        programaPage.editarParteDiario();

        maniobrasPage.crearCharlaSeguridad(opcionCharla, horaInicio, horaFin, obserOperacion, obserManiobra);

        //Assert Alerta Charla de Seguridad
        String msjAlertaCharlaSeguridad = maniobrasPage.getMsjeAlerta();
        Assert.assertEquals(msjAlertaCharlaSeguridad, "El registro ha sido guardado correctamente");
        maniobrasPage.aceptarAlert();

        maniobrasPage.agregarOperador(operador, responsable, ayudante1);
        maniobrasPage.subirRegistroCarrera();

        String msjAlertaGuardar = maniobrasPage.getMsjeAlerta();
        Assert.assertEquals(msjAlertaGuardar, "El registro ha sido guardado correctamente");
        maniobrasPage.aceptarAlert();

        maniobrasPage.verPrograma();

    }

    // CREAR MANIOBRA DE PESCA POSITIVA
    @Test(description="Maniobra pesca positiva", dataProvider="verProgramas")
    public void pescaPositiva(String username, String password, String estado, String tipoPrograma, String opcionPozo, String opcionPesca, String horaInicio, String horaFin, String obserOperacion, String mandril, String valvula, String caudal, String presion, String obserManiobra, String operador, String responsable, String ayudante1) throws InterruptedException {

        //Arrange
        LoginPage loginPage = new LoginPage(driver);
        IndexPage indexpage = new IndexPage(driver);
        VerProgramaPage verPrograma = new VerProgramaPage(driver);
        ProgramaPage programaPage = new ProgramaPage(driver);
        ManiobrasPage maniobrasPage = new ManiobrasPage(driver);

        //Actions
        loginPage.executeLoginOperador(username, password);
        indexpage.verProgramas();
        verPrograma.buscarPrograma(estado,tipoPrograma, opcionPozo);
        programaPage.editarParteDiario();
        maniobrasPage.crearPescaPositiva(opcionPesca, horaInicio, horaFin, obserOperacion, mandril, valvula, caudal, presion, obserManiobra);

        //Assert Alerta Maniobra
        String msjAlertaManiobra = maniobrasPage.getMsjeAlerta();
        Assert.assertEquals(msjAlertaManiobra, "El registro ha sido guardado correctamente");
        maniobrasPage.aceptarAlert();

        maniobrasPage.agregarOperador(operador, responsable, ayudante1);
        maniobrasPage.subirRegistroCarrera();

        String msjAlertaGuardar = maniobrasPage.getMsjeAlerta();
        Assert.assertEquals(msjAlertaGuardar, "El registro ha sido guardado correctamente");
        maniobrasPage.aceptarAlert();
        maniobrasPage.verPrograma();
    }

}