package web;

import com.everis.data.ExcelUtils;
import com.everis.global.Global;
import com.everis.po.*;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class BuscarIntervencionTest extends Base{

    Global global = new Global(driver);

    @DataProvider
    public Object[][] intervencionCases(ITestContext context) throws Exception{
        Object[][] testObjArray = ExcelUtils.getTableArray(super.getDataProviderDir()+context.getCurrentXmlTest().getParameter("dataProvider"),context.getCurrentXmlTest().getParameter("sheet"));
        return (testObjArray);
    }

    // BUSCAR INTERVENCIÓN
    @Test(description="Caso buscar intervencion", dataProvider="intervencionCases")
    public void buscarIntervencion(String username, String password, String opcionPozo, String estado) throws InterruptedException {

        //Arrange
        LoginPage loginPage = new LoginPage(driver);
        IndexPage indexpage = new IndexPage(driver);
        BuscarIntervencionPage buscarIntervencionPage = new BuscarIntervencionPage(driver);

        //Act
        loginPage.executeLoginIngeniero(username, password);
        indexpage.irABuscarIntervenciones();
        buscarIntervencionPage.buscarIntervencion(opcionPozo, estado);


    }
}
