package web;

import com.everis.data.ExcelUtils;
import com.everis.global.Global;
import com.everis.po.ReportesPage;
import com.everis.po.IndexPage;
import com.everis.po.LoginPage;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ReportesTest extends Base{

    Global global = new Global(driver);

    @DataProvider
    public Object[][] reporteDatosMemory(ITestContext context) throws Exception{
        Object[][] testObjArray = ExcelUtils.getTableArray(super.getDataProviderDir()+context.getCurrentXmlTest().getParameter("dataProvider"),context.getCurrentXmlTest().getParameter("sheet"));
        return (testObjArray);
    }

    // CONSULTA REPORTE DATOS
    @Test(description="Reporte Datos Memory", dataProvider="reporteDatosMemory")
    public void reporteDatosMemory(String username, String password, String distrito, String proyecto , String pozo) throws InterruptedException {

        //Arrange
        LoginPage loginPage = new LoginPage(driver);
        IndexPage indexpage = new IndexPage(driver);
        ReportesPage datosMemoryPage = new ReportesPage(driver);

        //Act
        loginPage.executeLoginIngeniero(username, password);
        indexpage.menuReportes();
        datosMemoryPage.reporteDatosMemory(distrito, proyecto , pozo);


    }

    @Test(description="Reporte Datos Memory Distrito", dataProvider="reporteDatosMemory")
    public void reporteDatosMemoryDistrito(String username, String password, String distrito) throws InterruptedException {

        //Arrange
        LoginPage loginPage = new LoginPage(driver);
        IndexPage indexpage = new IndexPage(driver);
        ReportesPage datosMemoryPage = new ReportesPage(driver);

        //Act
        loginPage.executeLoginIngeniero(username, password);
        indexpage.menuReportes();
        datosMemoryPage.reporteDatosMemoryDistrito(distrito);


    }
}
