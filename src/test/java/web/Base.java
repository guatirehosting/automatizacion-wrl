package web;

import com.everis.global.Global;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

public class Base {
	WebDriver driver;

	final Logger log = LogManager.getLogger(Global.class);

	private String seleniumUrl;
	private String driverPath;
	private String projectDir;
	private String dataProviderDir;
	private String driversDir;

	public Base() {
		this.projectDir = System.getProperty("user.dir");
		// this.dataProviderDir="D://repo//pae-ui//src//test//dataprovider//";
		this.dataProviderDir = projectDir + "//src//test//dataprovider//";
		this.driversDir = projectDir + "//src//test//resources//webDrivers//";
	}

	public String getProjectDir() {
		return projectDir;
	}

	public String getDataProviderDir() {
		return dataProviderDir;
	}

	public String getDriversDir() {
		return driversDir;
	}
	
	@BeforeMethod(alwaysRun = true)
	public void setupBeforeSuite(ITestContext context) {

		seleniumUrl = context.getCurrentXmlTest().getParameter("selenium.url");
		driverPath = driversDir + context.getCurrentXmlTest().getParameter("selenium.driver");

		//try {
			if (driverPath.contains("msedgedriver")) {
				System.setProperty("webdriver.edge.driver", driverPath);

				EdgeOptions edgeOptions = new EdgeOptions();
				edgeOptions.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
				edgeOptions.setCapability(CapabilityType.APPLICATION_NAME, true);

				driver = new EdgeDriver(edgeOptions);
				driver.manage().window().maximize();
				Global.setDriverInUse("msedgedriver");

			}
			else {
				System.setProperty("webdriver.chrome.driver", driverPath);
				// Initialize browser

				ChromeOptions ChromeOptions = new ChromeOptions();
				ChromeOptions.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
				driver = new ChromeDriver();
				driver.manage().window().maximize();

				Global.setDriverInUse("chromedriver");

			}

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

//		} catch (Exception e) {
//			log.info(e.getMessage());
//			throw new IllegalStateException("Can't start selenium browser", e);
//		}
		driver.get(seleniumUrl);

	}

	@AfterMethod(alwaysRun = true)
	public void setupAfterSuite() {
		driver.quit();
	}

}
