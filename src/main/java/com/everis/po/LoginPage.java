package com.everis.po;

import com.everis.global.Global;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class LoginPage extends Global {

    public LoginPage(WebDriver d) {
        super(d);
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "oValidadorUsuario.userName")
    WebElement inputUserName;

    @FindBy(id = "oValidadorUsuario.password")
    WebElement inputPassword;

    @FindBy(id = "botonLogin")
    WebElement btnLogin;

    @FindBy(xpath = "//h5[contains (.,'Bienvenido a Wireline, ')]")
    WebElement mensajeDeBienvenida;

    @FindBy(xpath = "//div[@id='divDetalleError']")
    WebElement msjErrorLogin;

    @FindBy(xpath = "//td[contains(text(),'(Administrador WRL)')]")
    WebElement msjLoginAdministradorWRL;

    @FindBy(xpath = "//td[contains(text(),'(GSJ) (Ingeniero)')]")
    WebElement msjeIngeniero;

    @FindBy(xpath = "//td[contains(text(),'ope6790 (GSJ) (Operador)')]")
    WebElement msjLoginOperador;

    @FindBy(xpath = "//td[contains(text(),'agg6790 (GSJ) (Supervisor)')]")
    WebElement msjLoginSupervisor;


    public void executeLogin(String username, String password){
        setUsername(username);
        setPassword(password);
        enviarLogin();
        Assert.assertEquals(mensajeDeBienvenida.getText(), "Bienvenido a Wireline, " + username + ".");
    }

    public void setUsername(String username){
        sendKeys(inputUserName, username, "Se ingresa el username");
    }

    public void setPassword(String password){
        sendKeys(inputPassword, password, "Se ingresa la password");
    }

    public void enviarLogin(){
        click(btnLogin, "Se clickea el boton de login");
    }



    public String getMensajedebienvenida() {
        return mensajeDeBienvenida.getText();
    }

    public String getmsjLoginAdministradorWRL() { return msjLoginAdministradorWRL.getText(); }

    public String getmsjIngeniero() { return msjeIngeniero.getText(); }


   public String getmsjErrorLogin() { return msjErrorLogin.getText(); }

    public void executeLoginIngeniero(String userNameIng, String passwordIng) {
        executeLogin(userNameIng, passwordIng);
        Assert.assertEquals(msjeIngeniero.getText(), userNameIng + " (GSJ) (Ingeniero)");
    }

    public void executeLoginAdmin(String username, String password) {
        executeLogin(username, password);
        Assert.assertEquals(msjLoginAdministradorWRL.getText(), username + " (GSJ) (Administrador WRL)");
    }

    public void executeLoginOperador(String username, String password) {
        executeLogin(username, password);
        Assert.assertEquals(msjLoginOperador.getText(), username + " (GSJ) (Operador)");
    }

    public void executeLoginSupervisor(String username, String password) {
        executeLogin(username, password);
        Assert.assertEquals(msjLoginSupervisor.getText(), username + " (GSJ) (Supervisor)");
    }

}
