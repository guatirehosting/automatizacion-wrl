package com.everis.po;

import com.everis.global.Global;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.io.File;
import java.util.ArrayList;

public class IntervencionPage extends Global {

    public IntervencionPage(WebDriver d) {
        super(d);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//select[@id='oInterv.motivoid']")
    WebElement selectMotivoIntervencion;

    @FindBy(xpath = "//select[@id='oInterv.pozoid']")
    WebElement we_selectPozo;

    @FindBy(xpath = "//select[@id='oInterv.tipoimputid']")
    WebElement we_Gasto;

    @FindBy(xpath = "//select[@id='oInterv.tipogasto']")
    WebElement we_Subtipo;

    @FindBy(xpath = "//input[@id='oInterv.fecha']")
    WebElement inputFecha;

    @FindBy(id = "oInterv.comentarios")
    WebElement inputComentarios;

    @FindBy(id = "GuardarButton")
    WebElement btnGuardar;

    @FindBy(id = "tbNewPrograma")
    WebElement btnNewPrograma;

    @FindBy(xpath = "//*[@id=\"oGridPrograma.registro[0].edit\"]/img")
    WebElement we_editarPrograma;

    @FindBy(xpath = "//input[@id='FinalizarButton']")
    WebElement btnFinalizar;

    @FindBy(xpath = "//textarea[@id='comentario_cierre']")
    WebElement input_ComentarioCierre;

    @FindBy(xpath = "/html/body/table/tbody/tr[3]/td/input[1]")
    WebElement btnAceptar;

    @FindBy(xpath = "//input[@id='FinOperativoButton']")
    WebElement btnFinOperativo;

    @FindBy(id = "comentario_fin_operativa")
    WebElement input_ComentarioFinOperativo;



    public void generarIntervencion(String opcionIntervencion, String opcionPozo, String tipoImp, String subTipo, String fecha, String comentarios) {

        select(selectMotivoIntervencion, opcionIntervencion, "Se selecciona en motivo de intervencion la opcion: " + opcionIntervencion);
        select(we_selectPozo, opcionPozo, "Se selecciona en Pozo la opcion: " + opcionPozo);
        select(we_Gasto, tipoImp, "Se selecciona en Gasto la opcion: " + tipoImp);
        select(we_Subtipo, subTipo, "Se selecciona en Subtipo la opcion: " + subTipo);
        sendKeys(inputFecha, fecha, "Se ingresa la fecha");
        sendKeys(inputComentarios, comentarios, "Se ingresan comentarios");
        click(btnGuardar, "Se clickea en boton Guardar");

    }

    public void generarAltaPrograma(){
        click(btnNewPrograma, "Se hace click en Alta de Programa");
    }

    public void editarProgramaAsociado(){
        click(we_editarPrograma, "Se hace click en editar Programas Asociados");
    }

    public void finalizarIntervencion (String comentario) throws InterruptedException {
        //AL SELECCIONAR FINALIZAR INTERVENCION, SE INGRESA UN COMENTARIO DE CIERRE Y SE ACEPTA UNA ALERTA

      String winHandleBefore = driver.getWindowHandle();

        click(btnFinalizar, "Se hace click en Finalizar Intervención");
        Alert alert = driver.switchTo().alert();
        alert.accept();

        String Tab1 = driver.getWindowHandle();
        ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles());
        if (!availableWindows.isEmpty()) {
            driver.switchTo().window(availableWindows.get(1));
        }
        /*Acciones en la nueva ventana*/
        sendKeys(input_ComentarioCierre, comentario, "Se ingresa un comentario de cierre");
        click(btnAceptar, "Se hace click en Aceptar");

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.switchTo().window(winHandleBefore);
        System.out.println("Pase la ventana ok");
        driver.close();

        Alert alert2 = driver.switchTo().alert();
        alert2.accept();

    }

    public void finOperativo(String comentario) throws InterruptedException {
        //AL SELECCIONAR EL BOTON FIN OPERATIVO, SE INGRESA UN COMENTARIO

        /* Ventana actual*/
        String winHandleBefore = driver.getWindowHandle();

        click(btnFinOperativo, "Se hace click en Fin Operativo");
        Alert alert = driver.switchTo().alert();
        alert.accept();


        /*Switch hacia la nueva ventana*/
        String Tab1 = driver.getWindowHandle();
        ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles());
        if (!availableWindows.isEmpty()) {
            Thread.sleep(5000);
            driver.switchTo().window(availableWindows.get(1));
        }


        /*Acciones en la nueva ventana*/
        sendKeys(input_ComentarioFinOperativo, comentario, "Se ingresa comentario de Fin Operativo");
        click(btnAceptar, "Se hace click en Aceptar");
        driver.quit();

        /*Volver a la ventana original*/
        driver.switchTo().window(winHandleBefore);


    }





}
