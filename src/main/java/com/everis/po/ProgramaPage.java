package com.everis.po;

import com.everis.global.Global;
import com.sun.codemodel.JTryBlock;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Instant;

public class ProgramaPage extends Global {

    public ProgramaPage(WebDriver d) {
        super(d);
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "oPrograma.tipoPrograma_id")
    WebElement selectTipoPrograma;

    @FindBy(id = "oPrograma.comentario")
    WebElement inputComentario;

    @FindBy(id = "btn_Guardar")
    WebElement btnGuardar;

    @FindBy(id = "oGridProgramaTareasRegulacion.registro[0].modelovalvula")
    WebElement id_TipoValvula1;

    @FindBy(id = "idProgramaAsignado")
    public WebElement id_Programa;

    @FindBy(id = "oGridProgramaTareasRegulacion.registro[1].modelovalvula")
    WebElement id_TipoValvula2;

    @FindBy(id = "btn_AprobarInicial")
    WebElement btnAprobar;

    @FindBy(xpath = "//img[@id='tbNew']")
    WebElement btnAltaParteDiario;

    @FindBy(xpath = "//div[@id='oDetailPD']//table//tbody//tr//td//table//tbody//tr//td//span[@class='combo datebox']//span//span[@class='combo-arrow']")
    WebElement select_calendario;

    @FindBy(xpath = "/html/body/div[2]/div/div[1]/div/div[2]/table/tbody/tr[3]/td[4]")
    WebElement select_fecha;

    @FindBy(xpath = "//body/form/div/table/tbody/tr/td/table/tbody/tr/td/div/div/form/div/table/tbody/tr/td[1]/input[1]")
    WebElement btnAgregar;

    @FindBy(id = "oGridPDiarios.registro[0].edit")
    WebElement btnEditParteDiario;

    @FindBy(xpath = "//input[@id='btn_Finalizar']")
    WebElement btnFinalizar;

    @FindBy(xpath = "//input[@id='btn_Aprobar']")
    WebElement btn_Aprobar;




    public void altaPrograma(String TipoPrograma, String comentarioPrograma) throws InterruptedException {

        select(selectTipoPrograma, TipoPrograma, "Se selecciona el tipo de programa: " + TipoPrograma);
        sendKeys(inputComentario, comentarioPrograma, "Se ingresan comentarios");
        click(btnGuardar, "Se clickea en boton Guardar");
    }

    public void ordenesDeTrabajo() {

        //UNA VEZ QUE SE DA DE ALTA UN PROGRAMA, SE COMPLETA LA ORDEN DE TRABAJO PARA EL OPERADOR
        //SE SELECCIONAN LAS VALVULAS Y SE SELECCIONA UNA OBSERVACIÓN

        String idPrograma = id_Programa.getText();
        WebElement inputCheckbox_1 = driver.findElement(By.xpath("//input[@id='chk_"+idPrograma+"_2_6']"));
        click(inputCheckbox_1, "Se hace click en el ultimo mandril");

        WebElement selectorValvula = driver.findElement(By.id("cmb_tpovalv_"+idPrograma+"_2_6"));
        String idValvula1 = id_TipoValvula1.getText();
        select(selectorValvula, idValvula1, "Se selecciona la valvula a bajar");

        WebElement observacion_1 = driver.findElement(By.xpath("//select[@id='cmb_justif_"+idPrograma+"_2_6']"));
        WebElement opcion3 = driver.findElement(By.xpath("//select[@id='cmb_justif_"+idPrograma+"_2_6']//option[contains(text(),'Por Perdida de herramienta')]"));
        String opcionObservacion = opcion3.getText();
        select(observacion_1, opcionObservacion, "Se selecciona la observacion");

        WebElement inputCheckbox_2 = driver.findElement(By.xpath("//input[@id='chk_"+idPrograma+"_2_9']"));
        click(inputCheckbox_2, "Se hace click en el ante ultimo mandril");

        WebElement selectorValvula2 = driver.findElement(By.id("cmb_tpovalv_"+idPrograma+"_2_9"));
        String idValvula2 = id_TipoValvula2.getText();
        select(selectorValvula2, idValvula2, "Se selecciona la valvula a bajar");

        WebElement observacion_2 = driver.findElement(By.xpath("//select[@id='cmb_justif_"+idPrograma+"_2_9']"));
        WebElement opcion2 = driver.findElement(By.xpath("//select[@id='cmb_justif_"+idPrograma+"_2_9']//option[contains(text(),'Adquisicion de datos')]"));
        String opcionObservacion2 = opcion2.getText();
        select(observacion_2, opcionObservacion2, "Se selecciona la observacion");

        click(btnGuardar, "Se clickea en boton Guardar");
    }

    public void aprobarPrograma() {
        click(btnAprobar, "Se clickea en boton Aprobar");
    }

    public void altaParteDiario() {
        click(btnAltaParteDiario, "Se da de alta un Parte Diario");
        click(select_calendario, "Se ingresa al calendario");
        click(select_fecha, "Se ingresa la fecha");
        click(btnAgregar, "Se hace click en boton Agregar");
    }

    public void editarParteDiario()  {
        click(btnEditParteDiario, "Se hace click en editar Parte Diario");
    }

    public void finalizarPrograma()  {
        click(btnFinalizar, "Se hace click en Finalizar programa");
    }

    public void aprobarIntervencion()  {
        click(btn_Aprobar, "Se hace click en Aprobar intervención");
    }







}
