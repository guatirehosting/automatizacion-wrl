package com.everis.po;

import com.everis.global.Global;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ReportesPage extends Global {

    public ReportesPage(WebDriver d) {
        super(d);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//select[@id='oDistritoCmb']")
    WebElement we_distrito;

    @FindBy(xpath = "//select[@id='oProyectoSecCmb']")
    WebElement we_proyecto;

    @FindBy(xpath = "//select[@id='filtro']")
    WebElement we_pozo;

    @FindBy(xpath = "//input[@id='boton']")
    WebElement btnEjecutar;

    public void reporteDatosMemory (String distrito,  String proyecto, String pozo) throws InterruptedException {

        select(we_distrito, distrito, "Se selecciona distrito en la opcion: " + distrito);
        select(we_proyecto, proyecto, "Se selecciona proyecto en la opcion: " + proyecto);
        select(we_pozo, pozo, "Se selecciona pozo en la opcion: " + pozo);

        click(btnEjecutar, "Se clickea en boton Ejecutar");

        By exportandoGif = (By.xpath("//a[@id='reloj']//img"));
        elementoInvisible(exportandoGif,300, "Se Descarga el Reporte Datos Memory");
    }

    public void reporteDatosMemoryDistrito (String distrito) throws InterruptedException {

        select(we_distrito, distrito, "Se selecciona distrito en la opcion: " + distrito);

        click(btnEjecutar, "Se clickea en boton Ejecutar");

        By exportandoGif = (By.xpath("//a[@id='reloj']//img"));
        elementoInvisible(exportandoGif,300, "Se Descarga el Reporte Datos Memory");
    }
}
