package com.everis.po;



import com.everis.global.Global;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConsultaPozoPage extends Global {

    public ConsultaPozoPage(WebDriver d) {
        super(d);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//select[@id='oDistritoCmb']")
    WebElement we_distrito;

    @FindBy(xpath = "//select[@id='oProyectoSecCmb']")
    WebElement we_proyecto;

    @FindBy(xpath = "//select[@id='Pozo']")
    WebElement we_pozo;

    @FindBy(xpath = "//input[@class='appButton']")
    WebElement bntBuscar;

    public void buscarHistoriaPozo(String distrito,  String proyecto, String pozo) throws InterruptedException {

        select(we_distrito, distrito, "Se selecciona distrito en la opcion: " + distrito);
        select(we_proyecto, proyecto, "Se selecciona proyecto en la opcion: " + proyecto);
        select(we_pozo, pozo, "Se selecciona pozo en la opcion: " + pozo);

        click(bntBuscar, "Se clickea en boton Buscar");

        Thread.sleep(5000);
    }
}
