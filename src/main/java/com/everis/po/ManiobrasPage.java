package com.everis.po;

import com.everis.global.Global;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.File;

public class ManiobrasPage extends Global {

    public ManiobrasPage(WebDriver d) {
        super(d);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//img[@id='tbNew3']")
    WebElement btnOperaciones;

    @FindBy(xpath = "//select[@id='oOperacion.maniobrasid']")
    WebElement selectManiobra;

    @FindBy(xpath = "//input[@id='oOperacion.horainicio']")
    WebElement inputHoraInicio;

    @FindBy(xpath = "//input[@id='oOperacion.horafin']")
    WebElement inputHoraFin;

    @FindBy(xpath = "//input[@id='oOperacion.observaciones']")
    WebElement inputObsOperacion;

    @FindBy(xpath = "//textarea[@id='oParteDiario.observacionesManiobras']")
    WebElement inputObsManiobra;

    @FindBy(xpath = "//tr[@id='filaIngresoOperacionBtn']//input[@class='appButton']")
    WebElement btnAgregar;

    @FindBy(xpath = "//img[@id='tbNew4']")
    WebElement btnFinal;

    @FindBy(xpath = "//textarea[@id='oPDFinal.operadorid']")
    WebElement inputOperador;

    @FindBy(xpath = "//select[@id='oPDFinal.responsableid']")
    WebElement selectResponsable;

    @FindBy(xpath = "//textarea[@id='oPDFinal.ayudante1id']")
    WebElement inputAyudante1;

    @FindBy(xpath = "//input[@id='SaveButton']")
    WebElement btnGuardar;

    @FindBy(xpath = "//*[@id=\"archivoSubir\"]")
    WebElement btnSubirArchivo;

    @FindBy(xpath = "//body[1]/form[1]/table[2]/tbody[1]/tr[2]/td[2]/div[1]/div[1]/table[2]/tbody[1]/tr[1]/td[1]/a[1]")
    WebElement we_VerPrograma;

    @FindBy(xpath = "//select[@id='oOperacion.mandril']")
    WebElement selectMandril;

    @FindBy(xpath = "//input[@id='oOperacion.nrovalvula']")
    WebElement inputValvula;

    @FindBy(xpath = "//input[@id='oOperacion.qpozo']")
    WebElement inputCaudalPozo;

    @FindBy(xpath = "//input[@id='oOperacion.ppozo']")
    WebElement inputPresionPozo;


    public void crearCharlaSeguridad(String opcionCharla, String horaInicio, String horaFin, String obserOperacion, String obserManiobra) throws InterruptedException {
        //SE CREA LA MANIOBRA CHARLA DE SEGURIDAD EN PARTE DIARIO
        click(btnOperaciones, "Se clickea en boton Operaciones");
        select(selectManiobra, opcionCharla, "Se selecciona la maniobra: " + opcionCharla);
        sendKeys(inputHoraInicio, horaInicio, "Se ingresa la hora de inicio");
        sendKeys(inputHoraFin, horaFin, "Se ingresa la hora de fin");
        sendKeys(inputObsOperacion, obserOperacion, "Se ingresa observacion de la operación");
        sendKeys(inputObsManiobra, obserManiobra, "Se ingresa observacion de la maniobra");
        click(btnAgregar, "Se clickea en boton Agregar");
    }

    public void crearPescaPositiva(String opcionPesca, String horaInicio, String horaFin, String obserOperacion, String mandril, String valvula, String caudal, String presion, String obserManiobra) throws InterruptedException {
        //SE CREA LA MANIOBRA PESCA POSITIVA EN UN MANDRIL LIBRE
        click(btnOperaciones, "Se clickea en boton Operaciones");
        select(selectManiobra, opcionPesca, "Se selecciona la maniobra: " + opcionPesca);
        sendKeys(inputHoraInicio, horaInicio, "Se ingresa la hora de inicio");
        sendKeys(inputHoraFin, horaFin, "Se ingresa la hora de fin");
        sendKeys(inputObsOperacion, obserOperacion, "Se ingresa observacion de la operación");
        select(selectMandril, mandril, "Se selecciona el mandril: " + mandril);
        sendKeys(inputValvula, valvula, "Se ingresa el numero de valvula");
        sendKeys(inputCaudalPozo, caudal, "Se ingresa el caudal del pozo");
        sendKeys(inputPresionPozo, presion, "Se ingresa la presion del pozo");
        sendKeys(inputObsManiobra, obserManiobra, "Se ingresa observacion de la maniobra");
        click(btnAgregar, "Se clickea en boton Agregar");
    }



    public void agregarOperador(String operador, String responsable, String ayudante1) throws InterruptedException {
        //SE AGREGA EL OPERADOR Y AYUDANTES QUE PARTICIPARON EN LA MANIOBRA DE PESCA

        click(btnFinal, "Se clickea en boton Final");
        sendKeys(inputOperador, operador, "Se ingresa operador");
        select(selectResponsable, responsable, "Se selecciona responsable: " + responsable);
        sendKeys(inputAyudante1, ayudante1, "Se ingresa ayudante");

    }

    public void subirRegistroCarrera() throws InterruptedException {
        //SE SUBE EL PDF DE REGISTRO DE CARRERA


       /* Ventana actual*/
        String winHandleBefore = driver.getWindowHandle();

        /*Accion que abre la nueva ventana*/
        click(btnSubirArchivo, "Se clickea en boton Final");

        /*Switch hacia la nueva ventana*/
        for(String winHandle : driver.getWindowHandles()){
            driver.switchTo().window(winHandle); }

        /*Acciones en la nueva ventana*/
        File file = new File("C:\\Users\\akearney\\Downloads\\PDF_PRUEBA.pdf");
        String path = file.getAbsolutePath();
        driver.findElement(By.xpath("//input[@id='F1']")).sendKeys(path);
        driver.findElement(By.xpath("//input[@class='appButton']")).click();

        /*Volver a la ventana original*/
        driver.switchTo().window(winHandleBefore);

        /*Guardar Acciones*/
        click(btnGuardar, "Se clickea en boton Guardar");

    }

    public void verPrograma() {

        click(we_VerPrograma, "Se hace click en Ver Programa");

    }



}
