package com.everis.po;

import com.everis.global.Global;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ISelect;

public class IndexPage extends Global {

    public IndexPage(WebDriver d) {
        super(d);
        PageFactory.initElements(driver, this);
    }

    // MENU INTERVENCIONES

    @FindBy(xpath = "//span[contains(text(),'Intervenciones')]")
    WebElement menuIntervencion;

    @FindBy(xpath = "//li[2]//ul[1]//li[1]//a[1]")
    WebElement btnBuscar;

    @FindBy(xpath = "//a[contains(text(),'Buscar c')]")
    WebElement btnBuscarTorre;

    @FindBy(xpath = "//li[2]//ul[1]//li[3]//a[1]")
    WebElement btnGenerar;

    @FindBy(xpath = "//a[contains(text(),'Generar c')]")
    WebElement btnGenerarEquipoTorre;

    @FindBy(xpath = "//a[contains(text(),'Generar Pozos Gas')]")
    WebElement btnGenerarPozoGas;

    @FindBy(xpath = "//a[contains(text(),'Revisión')]")
    WebElement btnRevisionDistribucion;

    @FindBy(xpath = "//a[contains(text(),'Ver Programas')]")
    WebElement btnVerProgramas;

    // MENU HERRAMIENTAS

    @FindBy(xpath = "//span[contains(text(),'Herramientas')]")
    WebElement menuHerramientas;

    @FindBy(xpath = "//a[contains(text(),'Agregar Objeto de pozo')]")
    WebElement btnAgregarObjetoPozos;

    @FindBy(xpath = "//a[contains(text(),'Archivos CCL')]")
    WebElement btnArchivosCCL;

    @FindBy(xpath = "//a[contains(text(),'Consulta Válvulas Memory')]")
    WebElement btnConsultaValvulasMemory;

    @FindBy(xpath = "//a[contains(text(),'Consulta de pozos')]")
    WebElement btnConsultadePozos;

    @FindBy(xpath = "//a[contains(text(),'Horas NPT')]")
    WebElement btnHorasNPT;

    @FindBy(xpath = "//a[contains(text(),'KM - Knowledge Management')]")
    WebElement btnKnowledgeManagement;

    @FindBy(xpath = "//a[contains(text(),'Regulación Inicial')]")
    WebElement btnRegulacionInicial;

    // MENU REPORTES

    @FindBy(xpath = "//span[contains(text(),'Reportes')]")
    WebElement menuReportes;

    @FindBy(xpath = "//a[contains(text(),'Datos Memory')]")
    WebElement btnReportesMemory;

    // MENU TABLAS
    @FindBy (xpath = "//span[contains(text(),'Tablas')]")
    WebElement menuTablas;

    @FindBy (xpath = "//a[contains(text(),'Tickets')]")
    WebElement btnTickets;

    public void crearIntervencion() {
        mouseHover(menuIntervencion, "se pone el mouse sobre el menu intervencion");
        click(btnGenerar, "Se clickea el botón Generar");
    }

    public void irABuscarIntervenciones() {
        mouseHover(menuIntervencion, "se pone el mouse sobre el menu intervencion");
        click(btnBuscar, "Se clickea el boton Buscar");
    }

    public void menuReportes() {
        mouseHover(menuReportes, "se pone el mouse sobre el menu reportes");
        click(btnReportesMemory, "Se clickea el boton Datos Memory");
    }

    public void buscarConsultadePozo() {
        mouseHover(menuHerramientas, "se pone el mouse sobre el menu intervencion");
        click(btnConsultadePozos, "Se clickea el boton Consulta de Pozos");
    }

    public void menuTickets(){
        mouseHover(menuTablas, "Se pone el mouse sobre el menu Tablas");
        click(btnTickets, "Se clickea el boton Tickets");
    }

    public void verProgramas(){
        mouseHover(menuIntervencion, "Se pone el mouse sobre el menu Intervencion");
        click(btnVerProgramas, "Se clickea el boton Ver Programas");
    }
}


