package com.everis.po;

import com.everis.global.Global;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class VerProgramaPage extends Global {

    public VerProgramaPage(WebDriver d) {
        super(d);
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "oEstadosProgCmb")
    WebElement selectEstado;

    @FindBy(xpath = "//select[@id='oPrograma.tipoPrograma_id']")
    WebElement selectTipoPrograma;

    @FindBy(xpath = "//select[@id='oPozosCmb']")
    WebElement selectPozo;

    @FindBy(xpath = "//input[@class='appButton']")
    WebElement btnBuscar;

    @FindBy(xpath = "//a[@id='oGridPrograma.registro.edit']//img")
    WebElement selectPrograma;


    public void buscarPrograma(String estado, String tipoPrograma, String opcionPozo) throws InterruptedException {
        //SE BUSCA UN PROGRAMA CON ESTADO ASIGNADO

        select(selectEstado, estado, "Se selecciona el estado: " + estado);
        select(selectTipoPrograma, tipoPrograma, "Se selecciona el tipo de programa: " + tipoPrograma);
        select(selectPozo, opcionPozo, "Se selecciona el pozo: " + opcionPozo);
        click(btnBuscar, "Se selecciona el boton Buscar");
        click(selectPrograma, "Se selecciona el programa");
    }

    public void buscarProgramaFinalizado (String estado2, String tipoPrograma, String opcionPozo) {
        //SE BUSCA PROGRAMA CON ESTADO FIN EJECUCION
        select(selectEstado, estado2, "Se selecciona el estado: " + estado2);
        select(selectTipoPrograma, tipoPrograma, "Se selecciona el tipo de programa: " + tipoPrograma);
        select(selectPozo, opcionPozo, "Se selecciona el pozo: " + opcionPozo);
        click(btnBuscar, "Se selecciona el boton Buscar");
    }

}