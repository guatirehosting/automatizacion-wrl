package com.everis.po;

import com.everis.global.Global;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.ArrayList;

public class TicketsPage extends Global {

    public TicketsPage(WebDriver d) {
        super(d);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//select[@id='oFuncionalidad']")
    WebElement id_funcionalidad;

    @FindBy(xpath = "//input[@id='oIdIntervFinOperativa']")
    WebElement id_intervencion;

    @FindBy(xpath = "//select[@id='equipoid']")
    WebElement id_equipo;

    @FindBy(xpath = "//input[@id='boton']")
    WebElement btnEjecutar;

    @FindBy (xpath = "//td[@class='appMensaje']")
    WebElement msgExito;

    public void asignarEquipoOffline (String funcionalidad , String intervencion, String equipo) throws InterruptedException {
        //SE AÑADE UN EQUIPO A LA ORDEN DE TRABAJO DEL PROGRAMA

        select(id_funcionalidad, funcionalidad, "Se selecciona funcionalidad en la opción: " + funcionalidad);
        sendKeys(id_intervencion , intervencion ,"Se ingresa id Intervencion " + intervencion);
        select(id_equipo , equipo , "Se ingresa equipo " + equipo );

        click(btnEjecutar, "Se clickea en boton Ejecutar");

        String Tab1 = driver.getWindowHandle();
        ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles());
        if (!availableWindows.isEmpty()) {
            driver.switchTo().window(availableWindows.get(1));
        }


        Assert.assertEquals(msgExito.getText(), "Se asignado el equipo a la intervencion 19622 correctamente");

    }


}
