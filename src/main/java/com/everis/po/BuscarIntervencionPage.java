package com.everis.po;

import com.everis.global.Global;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class BuscarIntervencionPage extends Global {

    public BuscarIntervencionPage(WebDriver d) {
        super(d);
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "q_pozo")
    WebElement inputPozo;

    @FindBy(xpath = "//div[@class='datagrid-cell-group']//select[@id='oEstadosIntervCmb']")
    WebElement selectEstado;

    @FindBy(xpath = "//body/form/div/div/div/div/div/div/table/tbody/tr/td/div/img[1]")
    WebElement btnVerIntervencion;

    public void buscarIntervencion(String opcionPozo, String estado) throws InterruptedException {

        clear(inputPozo);
        sendKeys(inputPozo, opcionPozo, "Se selecciona el pozo: " + opcionPozo);
        Thread.sleep(7000);
        select(selectEstado, estado, "Se selecciona el estado: " + estado);
        Thread.sleep(7000);


        click(btnVerIntervencion, "Se clickea en boton Ver Intervencion");

    }


}
