package com.everis.global;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import static io.restassured.RestAssured.given;

public class Global {
	protected WebDriver driver;
	public static final long TIMEWAIT = 30;

	public Global(WebDriver d) {
		this.driver = d;
	}

	public static Response getRequestResponse() {
		return requestResponse;
	}

	public static void setRequestResponse(Response requestResponse) {
		Global.requestResponse = requestResponse;
	}

	protected static Response requestResponse;

	public static String getDriverInUse() {
		return webDriver;
	}

	public static void setDriverInUse(String webDriver) {
		Global.webDriver = webDriver;
	}

	private static String webDriver;

	protected Boolean isVisibleinPage(By by) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		return wait.until(ExpectedConditions.visibilityOfElementLocated(by)).isDisplayed();
	}

	public void waitForVisibility(WebElement e) {
		WebDriverWait wait = new WebDriverWait(driver, TIMEWAIT);
		wait.until(ExpectedConditions.visibilityOf(e));
	}
	
	public void waitForInVisibility(String xpath) {
		WebDriverWait wait = new WebDriverWait(driver, TIMEWAIT);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xpath)));
	}
	

	public void clear(WebElement e) {
		waitForVisibility(e);
		e.clear();
	}

	public void click(WebElement e, String msg) {
		waitForVisibility(e);
		log().info(msg);
		e.click();
	}

	public void switchTo(WebElement e, String msg) {
		waitForVisibility(e);
		log().info(msg);
		driver.switchTo().frame(e);
	}


	public void sendKeys(WebElement e, String txt, String msg) {
		waitForVisibility(e);
		log().info(msg);
		e.sendKeys(txt);
	}

	public void select(WebElement element, String textoOpcion, String msg){
		Select selectObject = new Select(element);
		selectObject.selectByVisibleText(textoOpcion);
		log().info(msg);
	}

	public String getMsjeAlerta(){
		Alert alert = driver.switchTo().alert();
		String msjeAlerta = alert.getText();
		return msjeAlerta;
	}

	public void aceptarAlert(){
		Alert aceptarAlert =  driver.switchTo().alert();
		aceptarAlert.accept();
	}


	public String getText(WebElement e) {
		waitForVisibility(e);
		return e.getText();
	}

	public String reemplazarEnXpath(String xpath, String palabraACambiar, String... args) {
		for (int i = 0; i < args.length; i++) {
			xpath = xpath.replaceFirst(palabraACambiar, args[i]);
		}
		return xpath;
	}

	public void mouseHover(WebElement element, String msg) {
		Actions actions = new Actions(driver);
		actions.moveToElement(element).perform();
		log().info(msg);
	}

	public void elementoInvisible (By element, int segundos, String msg) {
		WebDriverWait wait = new WebDriverWait(driver, segundos);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(element));
		log().info(msg);
	}

	public void sendRequest(String tipoPeticion, String serviceName, String baseUrl, String pathRequest,
			String tokenKey, String tokenValue, String bodyRequest) {

		log().info("Se invoca API tipo: " + tipoPeticion + " de nombre: " + serviceName);

		if (tipoPeticion.equalsIgnoreCase("POST")) {

			// Se invoca el servicio
			Response response = given().contentType(ContentType.JSON).header(tokenKey, tokenValue).when()
					.body(bodyRequest).post(baseUrl + pathRequest);

			setRequestResponse(response);

			// Se obtiene la respuesta
			// JsonPath resp = getRequestResponse().jsonPath();
			// log().info(resp.get());
			// log().info(resp.get("result.pendingTyCRetailCount"));

		} else if (tipoPeticion.equalsIgnoreCase("GET")) {

			// Se invoca el servicio
			Response response = given().contentType(ContentType.JSON).header(tokenKey, tokenValue).when()
					.get(baseUrl + pathRequest);

			setRequestResponse(response);

		} else if (tipoPeticion.equalsIgnoreCase("DELETE")) {

			// Se invoca el servicio
			Response response = given().contentType(ContentType.JSON).header(tokenKey, tokenValue).when()
					.body(bodyRequest).delete(baseUrl + pathRequest);

			setRequestResponse(response);

		} else {

			// Se invoca el servicio
			Response response = given().contentType(ContentType.JSON).header(tokenKey, tokenValue).when()
					.body(bodyRequest).put(baseUrl + pathRequest);

			setRequestResponse(response);

		}
	}

	public Logger log() {
		return LogManager.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());
	}

}
